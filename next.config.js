const { hostname } = require("os");

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [{ hostname: "ik.imagekit.io" }],
  },
  env: {
    NEXTAUTH_SECRET: "ilHtLfi7FBIsmzF6wJIKj0rY4PJmZNgYkVQrcCEPYe8=",
    NEXTAUTH_URL: "http://localhost:3000",
    RESET_PASSWORD_URL: "http://localhost:3000/auth/resetPassword",
    SIGN_IN_URL: "http://localhost:3000/auth/signin",
    VERIFICATION_URL: "http://localhost:3000/verify",
    CLOUD_PUBLIC_KEY: "public_O5ALfHSoWva7uNJHmat4ZZ2jKps=",
    CLOUD_PRIVATE_KEY: "private_XJE0zhmoW3PvnhnWlHV8dn2pJFM=",
    CLOUD_URL_ENDPOINT: "https://ik.imagekit.io/ya70eyye2d",
    IMAGE_KIT_URL: "https://ik.imagekit.io",
  },
};

module.exports = nextConfig;
