import { Document, Model, Schema, model, models } from "mongoose";
import { compare, genSalt, hash } from "bcrypt";
import _ from "lodash";

interface UserDocument extends Document {
  email: string;
  name: string;
  password: string;
  role: "admin" | "user";
  avatar?: { url: string; id: string };
  verified: boolean;
}
interface Method {
  comparePassword(password: string): Promise<boolean>;
  compareEmail(email: string): Promise<boolean>;
}
const userSchema = new Schema<UserDocument, {}, Method>(
  {
    email: { type: String, required: true, unique: true },
    name: { type: String, required: true, trim: true },
    password: { type: String, required: true },
    role: { type: String, enum: ["admin", "user"], default: "user" },
    avatar: { type: Object, url: String, id: String },
    verified: { type: Boolean, url: String, id: String },
  },
  {
    timestamps: true,
  }
);

userSchema.pre("save", async function (next) {
  try {
    if (!this.isModified("password")) return next();
    const salt = await genSalt(10);
    this.password = await hash(this.password, salt);
  } catch (error) {
    throw new Error(error as any).message;
  }
});

userSchema.methods.comparePassword = async function (password) {
  try {
    return await compare(password, this.password);
  } catch (error) {
    throw Error(error as any).message;
  }
};
userSchema.methods.compareEmail = async function (email) {
  try {
    return (await email) === this.email ? true : false;
  } catch (error) {
    throw Error(error as any).message;
  }
};

const UserModel = models.User || model("User", userSchema);

export default UserModel as Model<UserDocument, {}, Method>;
