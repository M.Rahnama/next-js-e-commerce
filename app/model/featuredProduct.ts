import { Document, Model, model, models, Schema } from "mongoose";

interface FeaturedProductDocument extends Document {
  banner: { fileId: string; url: string };
  title: string;
  link: string;
  linkTitle: string;
}

const featuredProductSchema = new Schema<FeaturedProductDocument>({
  banner: {
    url: { type: String, required: true },
    fileId: { type: String, required: true },
  },
  title: { type: String, required: true },
  link: { type: String, required: true },
  linkTitle: { type: String, required: true },
});

const FeaturedProductModel =
  models.FeaturedProduct ||
  model<FeaturedProductDocument>("FeaturedProduct", featuredProductSchema);

export default FeaturedProductModel as Model<FeaturedProductDocument>;
