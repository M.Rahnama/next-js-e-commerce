"use server";

import startDb from "@/app/lib/db";
import UserModel from "@/app/model/userModel";
import { UserUpdateProfile } from "@/app/types";

export const updateProfileInfo = async (info: UserUpdateProfile) => {
  try {
    await startDb();
    await UserModel.findByIdAndUpdate(info.id, {
      name: info.name,
      avatar: info.avatar,
    });
  } catch (error) {
    throw new Error(`Sorry, ${(error as any).message}`);
  }
};
export const profileInfo = async (id: string) => {
  try {
    await startDb();
    const user = await UserModel.findById(id);
    if (!user) return false;
    return {
      id: user._id.toString(),
      name: user.name,
      email: user.email,
      avatar: user.avatar?.url,
      verified: user.verified,
    };
  } catch (error) {
    throw error;
  }
};
