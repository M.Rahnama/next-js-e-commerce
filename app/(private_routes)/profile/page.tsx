import ProfileForm from "@/app/components/ProfileForm";
import { getServerAuthSession } from "@/auth";
import React from "react";
import { profileInfo } from "./action";
import { redirect } from "next/navigation";
import { SessionUserProfile } from "@/app/types";
import Link from "next/link";

const Profile = async () => {
  const session = await getServerAuthSession();
  if (!session) return redirect("/auth/signin");

  const { avatar, email, id, name } = (await profileInfo(
    session.user.id
  )) as SessionUserProfile;

  return (
    <div className="flex py-4 space-y-4">
      <div className="border-r border-gray-700 p-4 space-y-4">
        <ProfileForm avatar={avatar!} email={email} id={id} name={name} />
      </div>
      <div className="p-4 flex-1">
        <div className="flex items-center justify-between">
          <h1 className="text-2xl font-semibold uppercase opacity-70 mb-4">
            Your recent orders
          </h1>
          <Link href="/profile/orders" className="uppercase hover:underline">
            See all orders
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Profile;
