import ImageKit from "imagekit";

const imageKit = new ImageKit({
  publicKey: process.env.CLOUD_PUBLIC_KEY!,
  privateKey: process.env.CLOUD_PRIVATE_KEY!,
  urlEndpoint: process.env.CLOUD_URL_ENDPOINT!,
});

export const authenticator = async () => {
  try {
    const response = await fetch(process.env.CLOUD_URL_ENDPOINT!);

    if (!response.ok) {
      const errorText = await response.text();
      throw new Error(
        `Request failed with status ${response.status}: ${errorText}`
      );
    }

    const data = await response.json();
    const { signature, expire, token } = data;
    return { signature, expire, token };
  } catch (error) {
    throw new Error(`Authentication request failed: ${(error as any).message}`);
  }
};

export const uploadImages = async (imageFile: File) => {
  const image = imageFile as File;
  const arrayBuffer = await image.arrayBuffer();
  const blob = new Blob([arrayBuffer]);
  const { url, fileId } = await imageKit.upload({
    file: blob as unknown as Buffer,
    fileName: image.name,
  });

  return { url, fileId };
};

export const formatPrice = (amount: number) => {
  const formatter = new Intl.NumberFormat("fa-IR", {
    style: "currency",
    currency: "IRR",
  });

  return formatter.format(amount);
};

export const dateFormat = (date: Date, format: string = "fa-IR") => {
  const formattedDate = new Intl.DateTimeFormat(format);

  return formattedDate.format(new Date(date));
};
