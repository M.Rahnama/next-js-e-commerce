"use client";
import axios from "axios";
import { notFound, useRouter } from "next/navigation";
import React, { useEffect } from "react";
import { toast } from "react-toastify";

type Props = {
  searchParams: { token: string; userId: string };
};

const Verify = (props: Props) => {
  const { userId, token } = props.searchParams;
  const router = useRouter();

  if (!userId || !token) notFound();

  useEffect(() => {
    axios
      .post("/api/verify", { userId, token })
      .then((response) => {
        const apiRes = response.data;
        const { message, error } = apiRes as { message: string; error: string };

        if (response.status === 200) {
          toast.success(message);
          return router.replace("/");
        }
      })
      .catch(({ response }) => {
        toast.error(response.data.error);
        return router.replace("/");
      });

    // empty dependency array means this effect will only run once (like componentDidMount in classes)
  }, []);
  return (
    <div className="text-3xl animate-pulse opacity-70">
      Please wait...
      <p>We are verifing your email</p>
    </div>
  );
};

export default Verify;
