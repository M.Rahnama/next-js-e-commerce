import React from "react";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

type Props = {
  position:
    | "top-right"
    | "top-center"
    | "top-left"
    | "bottom-right"
    | "bottom-center"
    | "bottom-left";
};

const Notification = () => {
  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        rtl={false}
        pauseOnHover
        theme="light"
      />
    </>
  );
};

export default Notification;
