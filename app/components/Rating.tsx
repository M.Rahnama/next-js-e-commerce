import React from "react";
import Star from "@Ui/Star";

interface Props {
  value: number;
}

export default function Rating({ value }: Props) {
  const data = Array(5).fill("");
  const fullStarts = Math.floor(value);
  const halfStar = value - fullStarts >= 0.1;
  return (
    <div className="flex items-center space-x-0.5">
      {data.map((_, index) => {
        return index + 1 <= fullStarts ? (
          //   <span>{` index==> ${index + 1}  fullStarts==> ${fullStarts}`}</span>
          <Star.Full key={index} />
        ) : halfStar && index === fullStarts ? (
          //   <span>{`halfStar==> ${halfStar} index==> ${index}  fullStarts==> ${fullStarts}`}</span>

          <Star.Half key={index} />
        ) : (
          // <span>{ `Empty ${ index }  fullStarts==> ${ fullStarts }` }</span>

          <Star.Empty key={index} />
        );
      })}
      <span className="font-semibold text-xs">{value}</span>
    </div>
  );
}
