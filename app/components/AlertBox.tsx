"use client";
import { Alert } from "@material-tailwind/react";
import { colors } from "@material-tailwind/react/types/generic";
import Link from "next/link";
import React from "react";
type Props = {
  title: string;
  hasLink: boolean;
  href: string;
  linkText: string;
  color?: colors;
};

const AlertBox = ({ color, title, hasLink, href, linkText }: Props) => {
  return (
    <Alert color={color}>
      <span className="font-semibold">{title} </span>
      {hasLink && <Link href={href}>{linkText}</Link>}
    </Alert>
  );
};

export default AlertBox;
