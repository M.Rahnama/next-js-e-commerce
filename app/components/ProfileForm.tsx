"use client";
import React, { useState, useTransition } from "react";
import { Button, Input } from "@material-tailwind/react";
import ProfileAvatarInput from "@Components/ProfileAvatarInput";
import { toast } from "react-toastify";
import { uploadImages } from "@Utils/helper";
import { UserUpdateProfile } from "../types";
import { updateProfileInfo } from "../(private_routes)/profile/action";
import { useRouter } from "next/navigation";

interface Props {
  avatar: string;
  name: string;
  email: string;
  id: string;
}

export default function ProfileForm({ id, name, avatar, email }: Props) {
  const [isPending, startTransition] = useTransition();
  const [avatarFile, setAvatarFile] = useState<File>();
  const [userName, setUserName] = useState(name);
  const router = useRouter();

  const avatarSource = avatarFile ? URL.createObjectURL(avatarFile) : avatar;
  const showSubmitButton = avatarSource !== avatar || userName !== name;

  const updateProfileHandler = async () => {
    if (userName.trim().length < 3) return toast.error("Invalid user name");

    const info: UserUpdateProfile = { id, name: userName };

    if (avatarFile) {
      const avatar = await uploadImages(avatarFile);
      info.avatar = avatar;
    }
    await updateProfileInfo(info);
    router.refresh();
  };
  return (
    <form
      className="space-y-6"
      action={() => {
        startTransition(async () => {
          await updateProfileHandler();
        });
      }}
    >
      <ProfileAvatarInput
        onChange={setAvatarFile}
        nameInitial={name[0]}
        avatar={avatarSource}
      />
      <div className="text-sm">Email: {email}</div>
      <Input
        crossOrigin={""}
        onChange={({ target }) => setUserName(target.value)}
        label="Name"
        value={userName}
        className="font-semibold"
      />
      {showSubmitButton ? (
        <Button
          placeholder={""}
          disabled={isPending}
          type="submit"
          className="w-full shadow-none hover:shadow-none hover:scale-[0.98]"
          color="blue"
        >
          Submit
        </Button>
      ) : null}
    </form>
  );
}
