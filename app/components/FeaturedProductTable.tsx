"use client";
import { Avatar, Button, CardBody, Typography } from "@material-tailwind/react";
import { truncate } from "lodash";
import Link from "next/link";
import React, { useTransition } from "react";
import { deleteFeaturedProductData } from "../(admin)/products/featured/update/action";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";

const TABLE_HEAD = ["Banner", "Detail product", "Product name", ""];

interface Props {
  products: Products[];
}

export interface Products {
  id: string;
  banner: string;
  title: string;
  link: string;
  linkTitle: string;
}

export default function FeaturedProductTable({ products }: Props) {
  const [isPending, startTransition] = useTransition();
  const router = useRouter();

  const handleDelete = async (id: string) => {
    try {
      await deleteFeaturedProductData(id);
      router.refresh();
    } catch (error) {
      toast.error((error as any).message);
    }
  };

  return (
    <div className="py-5">
      <CardBody placeholder={""} className="px-0">
        <table className="w-full min-w-max table-auto text-left">
          <thead>
            <tr>
              {TABLE_HEAD.map((head, index) => (
                <th
                  key={index}
                  className="border-y border-blue-gray-100 bg-blue-gray-50/50 p-4"
                >
                  <Typography
                    placeholder={""}
                    variant="small"
                    color="blue-gray"
                    className="font-normal leading-none opacity-70"
                  >
                    {head}
                  </Typography>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {products.map((item, index) => {
              const { id, link, title, banner } = item;
              const isLast = index === products.length - 1;
              const classes = isLast
                ? "p-4"
                : "p-4 border-b border-blue-gray-50";

              return (
                <tr key={id}>
                  <td className={classes}>
                    <div className="flex items-center gap-3">
                      <Link href={link}>
                        <Avatar
                          placeholder={""}
                          src={banner}
                          alt={title}
                          size="md"
                          variant="rounded"
                        />
                      </Link>
                    </div>
                  </td>
                  <td className={classes}>
                    <div className="flex items-center gap-3">
                      <Typography
                        placeholder={""}
                        variant="small"
                        color="blue-gray"
                        className="font-bold"
                      >
                        {truncate(title, { length: 100 })}
                      </Typography>
                    </div>
                  </td>
                  <td className={classes}>
                    <Link href={link}>
                      <Typography
                        placeholder={""}
                        variant="small"
                        color="blue-gray"
                        className="font-bold hover:underline"
                      >
                        View Product
                      </Typography>
                    </Link>
                  </td>
                  <td className={classes}>
                    <div className="flex items-center">
                      <Link
                        className="font-semibold uppercase text-xs text-blue-400 hover:underline"
                        href={`/products/featured/update?id=${id}`}
                      >
                        Edit
                      </Link>
                      <Button
                        onClick={() => {
                          startTransition(async () => {
                            await handleDelete(item.id);
                          });
                        }}
                        placeholder={""}
                        color="red"
                        ripple={false}
                        variant="text"
                        disabled={isPending}
                      >
                        {isPending ? "Deleting" : "Delete"}
                      </Button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </CardBody>
    </div>
  );
}
