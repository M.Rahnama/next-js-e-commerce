"use client";
import { getServerAuthSession } from "@/auth";
import axios from "axios";
import { useSession } from "next-auth/react";
import React from "react";
import { toast } from "react-toastify";

type Props = {};

const EmailVerificationAlert = (props: Props) => {
  const session = useSession();
  if (!session.data?.user.id || session.data.user.verified) return null;
  const handleRevefication = () => {
    axios
      .get(`/api/verify?userId=${session.data?.user.id}`)
      .then((response) => {
        const apiRes = response.data;
        const { message } = apiRes as { message: string };
        if (response.status === 200) {
          toast.success(message);
        }
      })
      .catch(({ response }) => {
        toast.error(response.data.error);
      });
  };
  return (
    <div className="p-2 text-center bg-blue-gray-50">
      <span>Please check your email to verification</span>
      <button
        className="underline mx-2 font-semibold"
        onClick={handleRevefication}
      >
        Get vrification link
      </button>
    </div>
  );
};

export default EmailVerificationAlert;
