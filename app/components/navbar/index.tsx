import React from "react";
import NavUi from "./NavUi";
import { getServerAuthSession } from "@/auth";
import CartModel from "@/app/model/cartModel";
import startDb from "@/app/lib/db";
import { Types } from "mongoose";
import { signIn } from "next-auth/react";
import { profileInfo } from "@/app/(private_routes)/profile/action";
import { redirect } from "next/navigation";
import { SessionUserProfile } from "@/app/types";

const getItemsCart = async () => {
  try {
    const session = await getServerAuthSession();
    if (!session?.user) return 0;
    const userId = session.user.id;
    await startDb();
    const cart = await CartModel.aggregate([
      { $match: { userId: new Types.ObjectId(userId) } },
      { $unwind: "$items" },
      {
        $group: {
          _id: "$_id",
          totalQuantity: { $sum: "$items.quantity" },
        },
      },
    ]);
    return cart.length ? cart[0].totalQuantity : 0;
  } catch (error) {
    console.log(
      "😜 ~ file: index.tsx:26 ~ getItemsCart ~ error:",
      (error as any).message
    );
  }
};
const Navbar = async () => {
  const session = await getServerAuthSession();

  const user = (await profileInfo(session?.user.id!)) as SessionUserProfile;

  const itemCount = await getItemsCart();
  return <NavUi cartItemsCount={itemCount} avatar={user.avatar!} />;
};

export default Navbar;
