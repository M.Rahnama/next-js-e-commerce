"use client";
import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Button,
  CardFooter,
  Chip,
} from "@material-tailwind/react";
import { truncate } from "lodash";
import Image from "next/image";
import Link from "next/link";
import useAuth from "../hook/useAuth";
import { useRouter } from "next/navigation";
import { useTransition } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import Rating from "./Rating";

interface Props {
  product: {
    id: string;
    title: string;
    description: string;
    category: string;
    thumbnail: string;
    sale: number;
    rating: number;
    price: {
      base: number;
      discounted: number;
    };
  };
}

export default function ProductCard({ product }: Props) {
  const [isPending, startTransition] = useTransition();

  const { loggedIn } = useAuth();
  const router = useRouter();

  const addToCart = async () => {
    if (!loggedIn) return router.push("/auth/signin");

    await axios
      .post("/api/product/cart", { productId: product.id, quantity: 1 })
      .then((response) => {
        const apiRes = response.data;
        const { success } = apiRes as { success: string };

        if (response.status === 200) {
          toast.success(success);
        }
      })
      .catch(({ response }) => {
        toast.error(response.data.error);
      });
    router.refresh();
  };
  return (
    <Card placeholder={""} className="w-full">
      <Link className="w-full" href={`/${product.title}/${product.id}`}>
        <CardHeader
          placeholder={""}
          shadow={false}
          floated={false}
          className="relative w-full aspect-square m-0"
        >
          <Image src={product.thumbnail} alt={product.title} fill />
          <div className="absolute right-0 p-2">
            <Chip color="red" value={`${product.sale}% off`} />
          </div>
        </CardHeader>
        <CardBody placeholder={""}>
          <div className="mb-2">
            <h3 className="line-clamp-1 font-medium text-blue-gray-800">
              {truncate(product.title, { length: 50 })}
            </h3>
          </div>
          <div className="flex justify-end items-center space-x-2 mb-2">
            <Typography
              placeholder={""}
              color="blue-gray"
              className="font-medium line-through"
            >
              ${product.price.base}
            </Typography>
            <Typography
              placeholder={""}
              color="blue-gray"
              className="font-medium"
            >
              ${product.price.discounted}
            </Typography>
          </div>
          <p className="font-normal text-sm opacity-75 line-clamp-3">
            {product.description}
          </p>
          {product.rating ? (
            <Rating value={parseFloat(product.rating.toFixed(1))} />
          ) : null}
        </CardBody>
      </Link>
      <CardFooter placeholder={""} className="pt-0 space-y-4">
        <Button
          onClick={() => {
            startTransition(async () => {
              await addToCart();
            });
          }}
          placeholder={""}
          ripple={false}
          fullWidth={true}
          disabled={isPending}
          className="bg-blue-gray-900/10 text-blue-gray-900 shadow-none hover:shadow-none hover:scale-105 focus:shadow-none focus:scale-105 active:scale-100"
        >
          Add to Cart
        </Button>
        <Button
          placeholder={""}
          ripple={false}
          fullWidth={true}
          className="bg-blue-400 text-white shadow-none hover:shadow-none hover:scale-105 focus:shadow-none focus:scale-105 active:scale-100"
        >
          Buy Now
        </Button>
      </CardFooter>
    </Card>
  );
}
