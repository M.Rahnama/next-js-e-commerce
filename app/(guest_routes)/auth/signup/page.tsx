import React from "react";
import SignupBox from "./components/SignupBox";

export default function SignUp() {
  return (
    <div className="flex w-full mt-10 justify-center items-center">
      <SignupBox />
    </div>
  );
}
