"use client";
import React, { useState } from "react";
import AuthFormContainer from "@Components/AuthFormContainer";
import { Button, Input, Typography } from "@material-tailwind/react";
import { SubmitHandler, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import { toast } from "react-toastify";
import { signIn } from "next-auth/react";

type Inputs = {
  name: string;
  email: string;
  password: string;
};

const SignupBox = () => {
  const [checkSubmit, setCheckSubmit] = useState(false);
  const schema = yup
    .object({
      name: yup.string().required("Your entry name is incorrect"),
      email: yup
        .string()
        .email("Email or password incorect")
        .required("Your email field is empty!"),
      password: yup.string().required("Email or password incorect"),
    })
    .required();
  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm<Inputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    const result = await axios
      .post("/api/user", data)
      .then((response) => {
        const apiRes = response.data;
        const { message } = apiRes as { message: string };
        if (response.status === 200) {
          toast.success(message);
          setCheckSubmit(true);
          signIn("credentials", {
            email: data.email,
            password: data.password,
          });
        }
      })
      .catch(({ response }) => {
        toast.error(response.data.error);
        setCheckSubmit(false);
      });

    return result;
  };

  return (
    <AuthFormContainer
      onSubmit={handleSubmit(onSubmit)}
      title="Create New Account"
    >
      <Input crossOrigin={""} label="Name" {...register("name")} />
      {errors.name && (
        <Typography
          placeholder={""}
          variant="small"
          color="gray"
          className="mt-2 flex text-xs text-red-500 items-center gap-1 font-normal"
        >
          {errors.name?.message}
        </Typography>
      )}
      <Input crossOrigin={""} label="Email" {...register("email")} />

      <Input
        crossOrigin={""}
        {...register("password")}
        label="Password"
        type="password"
      />
      {errors.email && (
        <Typography
          placeholder={""}
          variant="small"
          color="gray"
          className="mt-2 flex text-xs text-red-500 items-center gap-1 font-normal"
        >
          {errors.password?.message}
        </Typography>
      )}
      <Button
        placeholder={""}
        disabled={checkSubmit}
        color="blue"
        type="submit"
        className="w-full"
      >
        Sign up
      </Button>
    </AuthFormContainer>
  );
};

export default SignupBox;
