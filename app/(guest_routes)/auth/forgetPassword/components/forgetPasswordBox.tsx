"use client";

"use client";
import React, { useState } from "react";
import { Button, Input } from "@material-tailwind/react";
import { useFormik } from "formik";
import * as yup from "yup";
import Link from "next/link";
import AuthFormContainer from "@/app/components/AuthFormContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { SubmitHandler, useForm } from "react-hook-form";
import axios from "axios";
import { toast } from "react-toastify";

export default function ForgetPasswordBox() {
  const [checkSubmit, setCheckSubmit] = useState(false);
  type Inputs = {
    email: string;
  };

  const schema = yup
    .object({
      email: yup
        .string()
        .email("Email or password incorect")
        .required("Your email field is empty!"),
    })
    .required();

  const { register, handleSubmit } = useForm<Inputs>({
    resolver: yupResolver(schema),
  });
  const handleOnSubmit: SubmitHandler<Inputs> = async (data) => {
    const result = await axios
      .post("/api/user/forgetPassword", data)
      .then((response) => {
        const apiRes = response.data;
        const { message } = apiRes as { message: string };

        if (response.status === 200) {
          toast.success(message);
          setCheckSubmit(true);
        }
      })
      .catch(({ response }) => {
        toast.error(response.data.error);
        setCheckSubmit(false);
      });

    return result;
  };

  return (
    <AuthFormContainer
      title="Forget password"
      onSubmit={handleSubmit(handleOnSubmit)}
    >
      <Input crossOrigin={""} label="Email" {...register("email")} />
      <Button
        placeholder={""}
        type="submit"
        color="blue"
        className="w-full"
        disabled={checkSubmit}
      >
        Send Link
      </Button>
      <div className="flex items-center justify-between">
        <Link href="/auth/signin">Sign in</Link>
        <Link href="/auth/signup">Sign up</Link>
      </div>
    </AuthFormContainer>
  );
}
