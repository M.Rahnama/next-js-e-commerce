import React from "react";
import ForgetPasswordBox from "./components/forgetPasswordBox";

const ForgetPassword = () => {
  return (
    <div className="flex w-full mt-10 justify-center items-center">
      <ForgetPasswordBox />
    </div>
  );
};

export default ForgetPassword;
