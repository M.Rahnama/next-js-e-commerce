import passwordResetToken from "@/app/model/passwordResetToken";
import { notFound } from "next/navigation";
import React from "react";
import UpdatePassword from "./components/UpdatePassword";
type props = {
  searchParams: {
    token: string;
    userId: string;
  };
};

const fetchTokenValidation = async (token: string, userId: string) => {
  const resetToken = await passwordResetToken.findOne({ user: userId });
  if (!resetToken) return null;

  const resetPassMatch = await resetToken.compareToken(token);
  if (!resetPassMatch) return null;

  return true;
};

export default async function ResetPassword({ searchParams }: props) {
  const { token, userId } = searchParams;

  if (!token || !userId) notFound();

  const isValid = await fetchTokenValidation(token, userId);

  if (!isValid) notFound();

  return (
    <div className="flex w-full mt-10 justify-center items-center">
      <UpdatePassword token={token} userId={userId} />
    </div>
  );
}
