"use client";
import React from "react";
import FormContainer from "@/app/components/AuthFormContainer";
import { Button, Input } from "@material-tailwind/react";
import * as yup from "yup";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";

interface Props {
  userId: string;
  token: string;
}
type Inputs = {
  password1: string;
  password2: string;
};

export default function UpdatePassword({ token, userId }: Props) {
  const router = useRouter();
  const schema = yup
    .object({
      password1: yup.string().required("Password is required"),
      password2: yup
        .string()
        .oneOf([yup.ref("password1")], "Passwords must match")
        .required("Confirm password is required"),
    })
    .required();
  const { register, handleSubmit } = useForm<Inputs>({
    resolver: yupResolver(schema),
  });

  const handleOnSubmit = async (data: any) => {
    const result = await axios
      .post("/api/user/updatePassword", {
        password: data.password1,
        token,
        userId,
      })
      .then((response) => {
        const apiRes = response.data;
        const { message } = apiRes as { message: string };

        if (response.status === 200) {
          toast.success(message);
          router.replace("/auth/signin");
        }
      })
      .catch(({ response }) => {
        toast.error(response.data.error);
      });
    return result;
  };
  return (
    <FormContainer
      title="Reset password"
      onSubmit={handleSubmit(handleOnSubmit)}
    >
      <Input {...register("password1")} crossOrigin={""} label="Password" />
      <Input
        crossOrigin={""}
        label="Confirm Password"
        {...register("password2")}
      />
      <Button placeholder={""} type="submit" className="w-full" color="blue">
        Reset Password
      </Button>
    </FormContainer>
  );
}
