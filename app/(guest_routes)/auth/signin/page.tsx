import React from "react";
import SignInBox from "./components/signinBox";

export default function SignIn() {
  return (
    <div className="flex w-full mt-10 justify-center items-center">
      <SignInBox />
    </div>
  );
}
