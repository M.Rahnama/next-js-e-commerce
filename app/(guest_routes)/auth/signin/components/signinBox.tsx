"use client";

import { Button, Input, Typography } from "@material-tailwind/react";
import AuthFormContainer from "@Components/AuthFormContainer";
import React, { useState } from "react";
import Link from "next/link";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { signIn } from "next-auth/react";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";

const SignInBox = () => {
  const router = useRouter();
  const [checkSubmit, setCheckSubmit] = useState(false);

  type Inputs = {
    email: string;
    password: string;
  };

  const schema = yup
    .object({
      email: yup
        .string()
        .email("Email or password incorect")
        .required("Your email field is empty!"),
      password: yup.string().required("Email or password incorect"),
    })
    .required();

  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm<Inputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data: any) => {
    const signInResult = await signIn("credentials", {
      email: data.email,
      password: data.password,
      redirect: false,
    });
    if (signInResult?.error === "CredentialsSignin") {
      toast.error("Email/Password is incorect !");
      setCheckSubmit(false);
    }

    if (!signInResult?.error) {
      router.refresh();
      setCheckSubmit(true);
    }
  };

  return (
    <AuthFormContainer
      title="Create New Account"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Input crossOrigin={""} label="Email" {...register("email")} />

      <Input
        crossOrigin={""}
        {...register("password")}
        label="Password"
        type="password"
      />
      {errors.email && (
        <Typography
          placeholder={""}
          variant="small"
          color="gray"
          className="mt-2 flex text-xs text-red-500 items-center gap-1 font-normal"
        >
          {errors.password?.message}
        </Typography>
      )}
      <Button
        placeholder={""}
        disabled={checkSubmit}
        color="blue"
        type="submit"
        className="w-full"
      >
        Sign In
      </Button>
      <div className="flex items-center justify-between">
        <Link href="/auth/signup">Sign up</Link>
        <Link href="/auth/forgetPassword">Forget password</Link>
      </div>
    </AuthFormContainer>
  );
};

export default SignInBox;
