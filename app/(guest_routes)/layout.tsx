import { getServerAuthSession } from "@/auth";
import { redirect } from "next/navigation";
import React, { ReactNode } from "react";
import Navbar from "../components/navbar";

type Props = {
  children: ReactNode;
};

const GuestLayout = async ({ children }: Props) => {
  const session = await getServerAuthSession();
  if (session) {
    return redirect("/");
  }
  return (
    <div>
      <Navbar />
      {children}
    </div>
  );
};

export default GuestLayout;
