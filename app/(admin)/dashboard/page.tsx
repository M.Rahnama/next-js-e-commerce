import Rating from "@/app/components/Rating";
import React from "react";

type Props = {};

const Dashboard = (props: Props) => {
  return (
    <div>
      <Rating value={3.5} />
    </div>
  );
};

export default Dashboard;
