import { getServerAuthSession } from "@/auth";
import { redirect } from "next/navigation";
import React, { ReactNode } from "react";
import AdminSidebar from "../components/AdminSidebar";

type Props = {
  children: ReactNode;
};

const AdminLayout = async ({ children }: Props) => {
  const session = await getServerAuthSession();
  const isAdmin = session?.user.role === "admin";
  if (!isAdmin) return redirect("/auth/signin");
  return (
    <div>
      <AdminSidebar>{children}</AdminSidebar>
    </div>
  );
};

export default AdminLayout;
