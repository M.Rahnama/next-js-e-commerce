"use client";

import React from "react";
import ProductForm from "../_components/ProductForm";
import { ProductInfo } from "@/app/types";

import { newProductInfoSchema } from "@/app/utils/productValidation";
import { ValidationError } from "yup";
import { toast } from "react-toastify";
import { createProduct } from "../action";
import { uploadImages } from "@/app/utils/helper";
import { useRouter } from "next/navigation";

const CreateProduct = () => {
  const { refresh, push } = useRouter();
  const validateAndCreateProduct = async (values: ProductInfo) => {
    try {
      await newProductInfoSchema.validate(values, { abortEarly: false });
      const images = values.images;
      let productImages: { fileId: string; url: string }[] = [];
      const thumbnailImage = await uploadImages(values.thumbnail!);
      if (images) {
        const uploadPromise = images.map(async (imageFile) => {
          const { url, fileId } = await uploadImages(imageFile);
          return { url, fileId };
        });
        productImages = await Promise.all(uploadPromise);
      }

      await createProduct({
        ...values,
        price: {
          base: values.mrp,
          discounted: values.salePrice,
        },
        thumbnail: thumbnailImage,
        images: productImages,
      });
      refresh();
      push("/products");
    } catch (error) {
      console.log(
        "😜 ~ file: page.tsx:54 ~ validateAndCreateProduct ~ error:",
        error
      );
      if (error instanceof ValidationError) {
        error.inner.map((err) => {
          toast.error(err.message);
        });
      }
    }
  };

  return (
    <div>
      <ProductForm onSubmit={validateAndCreateProduct} />
    </div>
  );
};

export default CreateProduct;
