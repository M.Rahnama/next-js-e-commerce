import FeaturedProductForm from "@/app/components/FeaturedProductForm";
import FeaturedProductTable from "@/app/components/FeaturedProductTable";
import React from "react";
import { fetchBanner } from "./action";

const AddFeaturedProduct = async () => {
  const products = await fetchBanner();

  return (
    <div>
      <FeaturedProductForm />
      <FeaturedProductTable products={products} />
    </div>
  );
};

export default AddFeaturedProduct;
