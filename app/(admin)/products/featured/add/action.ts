"use server";

import { Products } from "@/app/components/FeaturedProductTable";
import startDb from "@/app/lib/db";
import FeaturedProductModel from "@/app/model/featuredProduct";
import { NewFeaturedProduct } from "@App/types";

export const createFeaturedProduct = async (info: NewFeaturedProduct) => {
  try {
    await startDb();
    await FeaturedProductModel.create({ ...info });
  } catch (error) {
    console.log((error as any).message);
    throw new Error("Something went wrong, can not create featured product!");
  }
};

export const fetchBanner = async (): Promise<Products[]> => {
  try {
    await startDb();
    const productsList = await FeaturedProductModel.find().sort("-createdAt");

    return productsList.map((product): Products => {
      return {
        id: product._id.toString(),
        banner: product.banner.url,
        link: product.link,
        linkTitle: product.linkTitle,
        title: product.title,
      };
    });
  } catch (error) {
    console.log((error as any).message);
    throw new Error(
      "Something went wrong, can not fetch featured product banner!"
    );
  }
};
