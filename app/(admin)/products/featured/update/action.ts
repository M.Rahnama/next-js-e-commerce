"use server";

import startDb from "@App/lib/db";
import { FeaturedProductForUpdate } from "@App/types";
import FeaturedProductModel from "@App/model/featuredProduct";
import { isValidObjectId } from "mongoose";
import { redirect } from "next/navigation";

export const updateFeaturedProducts = async (id: string) => {
  try {
    if (!isValidObjectId(id)) return redirect("/404");

    await startDb();
    const product = await FeaturedProductModel.findById(id);
    if (!product) redirect("/404");
    const { _id, title, linkTitle, link, banner } = product;
    return {
      id: _id.toString(),
      title,
      linkTitle,
      link,
      banner: banner.url,
    };
  } catch (error) {
    console.log((error as any).message);
    throw new Error("Something went wrong, can not update featured product!");
  }
};

export const updateFeaturedProductData = async (
  id: string,
  info: FeaturedProductForUpdate
) => {
  try {
    await startDb();
    await FeaturedProductModel.findByIdAndUpdate(id, { ...info });
  } catch (error) {
    console.log((error as any).message);
    throw new Error("Something went wrong, can not update featured product!");
  }
};
export const deleteFeaturedProductData = async (id: string) => {
  try {
    await startDb();
    await FeaturedProductModel.findByIdAndDelete(id);
  } catch (error) {
    console.log((error as any).message);
    throw new Error("Something went wrong, can not update featured product!");
  }
};
