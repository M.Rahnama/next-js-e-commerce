import FeaturedProductForm from "@/app/components/FeaturedProductForm";
import React from "react";
import { updateFeaturedProducts } from "./action";

type Props = { searchParams: { id: string } };

const UpdateFeaturedProduct = async ({ searchParams }: Props) => {
  const { id } = searchParams;

  const product = await updateFeaturedProducts(id);

  return <FeaturedProductForm initialValue={product} />;
};

export default UpdateFeaturedProduct;
