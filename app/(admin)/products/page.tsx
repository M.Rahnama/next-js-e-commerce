import ProductTable, {
  Product,
} from "@/app/(admin)/products/_components/ProductTable";
import startDb from "@/app/lib/db";
import ProductModel from "@/app/model/productModel";
import React from "react";
import { fetchProduct } from "./action";

const Products = async () => {
  const products = await fetchProduct(1, 10);
  return (
    <div className="max-w-screen-lg mx-auto p-4 xl:0">
      <ProductTable currentPageNo={0} products={products} />
    </div>
  );
};

export default Products;
