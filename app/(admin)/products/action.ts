"use server";

const imageKit = new ImageKit({
  publicKey: process.env.CLOUD_PUBLIC_KEY!,
  privateKey: process.env.CLOUD_PRIVATE_KEY!,
  urlEndpoint: process.env.CLOUD_URL_ENDPOINT!,
});

import ProductModel from "@/app/model/productModel";
import startDb from "@App/lib/db";
import {
  NewProduct,
  ProductInfo,
  ProductResponse,
  ProductToUpdate,
} from "@App/types";
import ImageKit from "imagekit";
import { isValidObjectId } from "mongoose";
import { redirect } from "next/navigation";
import { Product } from "./_components/ProductTable";

export const createProduct = async (info: NewProduct) => {
  try {
    await startDb();
    await ProductModel.create({ ...info });
  } catch (error) {
    console.log((error as any).message);
    throw new Error("Something went wrong, can not create product!");
  }
};

export const productFetch = async (productId: string): Promise<string> => {
  if (!isValidObjectId(productId)) return redirect("/404");

  await startDb();
  const product = await ProductModel.findById(productId);
  if (!product) return redirect("/404");

  const finalProduct: ProductResponse = {
    id: product._id.toString(),
    title: product.title,
    thumbnail: product.thumbnail,
    images: product.images?.map(({ id, url, fileId }) => ({
      id,
      fileId,
      url,
    })),
    description: product.description,
    price: product.price,
    bulletPoints: product.bulletPoints,
    category: product.category,
    quantity: product.quantity,
  };
  return JSON.stringify(finalProduct);
};

export const removeImage = async (fileId: string) => {
  await imageKit.deleteFile(fileId, function (error, result) {
    if (error) console.log(error);
    else console.log(result);
  });
};

export const removeAndUpdateProductImage = async (
  id: string,
  fileId: string
) => {
  try {
    const { $ResponseMetadata } = await imageKit.deleteFile(fileId);

    if ($ResponseMetadata.statusCode === 204) {
      await startDb();
      await ProductModel.findByIdAndUpdate(id, {
        $pull: { images: { fileId } },
      });
    }
  } catch (error) {
    console.log("Error from removing image file:", (error as any).message);
    throw error;
  }
};

export const updateProduct = async (
  id: string,
  productInfo: ProductToUpdate
) => {
  try {
    await startDb();
    let images: typeof productInfo.images = [];
    if (productInfo.images) {
      images = productInfo.images;
    }
    delete productInfo.images;
    await ProductModel.findByIdAndUpdate(id, {
      ...productInfo,
      $push: { images },
    });
  } catch (error) {
    console.log("Error in update product:", (error as any).message);
    throw error;
  }
};

export const fetchProduct = async (
  pageNo: number,
  perPage: number
): Promise<Product[]> => {
  const skipCount = (pageNo - 1) * perPage;
  await startDb();
  const productsList = await ProductModel.find()
    .sort("-createdAt")
    .skip(skipCount)
    .limit(perPage);
  return productsList.map((product): Product => {
    return {
      id: product._id.toString(),
      title: product.title,
      thumbnail: product.thumbnail.url,
      description: product.description,
      price: {
        mrp: product.price.base,
        salePrice: product.price.discounted,
        saleOff: product.sale,
      },
      category: product.category,
      quantity: product.quantity,
    };
  });
};
