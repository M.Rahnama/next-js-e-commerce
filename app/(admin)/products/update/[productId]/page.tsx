import React from "react";
import UpdateProudct from "../../_components/updateProudct";
import { productFetch } from "../../action";

type Props = {
  params: { productId: string };
  searchParams: any;
};

const UpdateProduct = async (prop: Props) => {
  const { productId } = prop.params;
  const product = await productFetch(productId);

  return (
    <>
      <UpdateProudct product={JSON.parse(product)} />
    </>
  );
};

export default UpdateProduct;
