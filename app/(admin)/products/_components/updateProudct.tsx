"use client";

import { ProductInfo, ProductResponse, ProductToUpdate } from "@App/types";
import React from "react";
import ProductForm, { InitialValue } from "./ProductForm";
import {
  removeAndUpdateProductImage,
  removeImage,
  updateProduct,
} from "../action";
import { updateProductInfoSchema } from "@/app/utils/productValidation";
import { uploadImages } from "@/app/utils/helper";
import { useRouter } from "next/navigation";
import { ValidationError } from "yup";
import { toast } from "react-toastify";

type Props = {
  product: ProductResponse;
};

const UpdateProudct = ({ product }: Props) => {
  const { refresh, push } = useRouter();
  const initialValue: InitialValue = {
    ...product,
    images: product.images?.map((img) => img.url),
    fileId: product.images?.map((img) => img.fileId),
    thumbnail: product.thumbnail.url,
    mrp: product.price.base,
    salePrice: product.price.discounted,
    bulletPoints: product.bulletPoints || [],
  };
  const handleRemoveImage = (src: string) => {
    const url = new URL(src);
    const index = initialValue.images?.findIndex((img) => img === url.href);

    if (index === -1) {
      return;
    }
    const fileId = initialValue.fileId?.[index!];
    removeAndUpdateProductImage(product.id, fileId!);
  };
  const handleOnSubmit = async (values: ProductInfo) => {
    const { thumbnail, images } = values;
    try {
      await updateProductInfoSchema.validate(values, { abortEarly: false });

      const dataToUpdate: ProductToUpdate = {
        title: values.title,
        description: values.description,
        bulletPoints: values.bulletPoints,
        category: values.category,
        quantity: values.quantity,
        price: {
          base: values.mrp,
          discounted: values.salePrice,
        },
      };
      if (thumbnail) {
        await removeImage(product.thumbnail.fileId);
        const { url, fileId } = await uploadImages(thumbnail);
        dataToUpdate.thumbnail = { url, fileId };
      }
      if (images.length) {
        const imagePromise = images.map(async (img) => {
          return await uploadImages(img);
        });
        dataToUpdate.images = await Promise.all(imagePromise);
      }
      console.log(
        "😜 ~ file: updateProudct.tsx:78 ~ handleOnSubmit ~ dataToUpdate:",
        dataToUpdate
      );

      await updateProduct(product.id, dataToUpdate);
      refresh();
      push("/products");
    } catch (error) {
      console.log(
        "😜 ~ file: page.tsx:54 ~ validateAndCreateProduct ~ error:",
        error
      );
      if (error instanceof ValidationError) {
        error.inner.map((err) => {
          toast.error(err.message);
        });
      }
    }
  };

  return (
    <ProductForm
      onImageRemove={handleRemoveImage}
      onSubmit={handleOnSubmit}
      initialValue={initialValue}
    />
  );
};

export default UpdateProudct;
