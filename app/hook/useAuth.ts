import { useSession } from "next-auth/react";
import { SessionUserProfile } from "../types";

type Props = {
  isAdmin: boolean;
  loading: boolean;
  loggedIn: boolean;
  profile?: SessionUserProfile | null;
};

const useAuth = (): Props => {
  const session = useSession();
  const user = session.data?.user;

  return {
    loading: session.status === "loading",
    loggedIn: session.status === "authenticated",
    isAdmin: user?.role === "admin",
    profile: user,
  };
};

export default useAuth;
