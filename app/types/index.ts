import { ObjectId } from "mongoose";
import React from "react";

export interface MenuItems {
  href: string;
  icon: React.JSX.Element;
  label: string;
}

export interface NewUser {
  name: string;
  email: string;
  password: string;
}
export interface SessionUserProfile {
  id: string;
  name: string;
  email: string;
  avatar?: string;
  role: "user" | "admin";
  verified: boolean;
}
export interface UserUpdateProfile {
  id: string;
  avatar?: { fileId: string; url: string };
  name: string;
}
export interface SignInCredentials {
  email: string;
  password: string;
}
export interface ForgetPassword {
  email: string;
}
export interface EmailVerifyRequest {
  token: string;
  userId: string;
}
export interface UpdatePasswordRequest {
  password: string;
  token: string;
  userId: string;
}
export interface ProductInfo {
  title: string;
  description: string;
  bulletPoints: string[];
  mrp: number;
  salePrice: number;
  category: string;
  quantity: number;
  thumbnail?: File;
  images: File[];
}
export interface ProductType {
  id: string;
  title: string;
  description: string;
  category: string;
  thumbnail: string;
  rating: number;
  price: {
    base: number;
    discounted: number;
  };
  sale: number;
}
export interface NewProduct {
  title: string;
  description: string;
  bulletPoints?: string[];
  thumbnail: { url: string; fileId: string };
  images?: { url: string; fileId: string }[];
  price: {
    base: number;
    discounted: number;
  };
  category: string;
  quantity: number;
  rating?: number;
}
export interface ProductToUpdate {
  title: string;
  description: string;
  bulletPoints: string[];
  category: string;
  quantity: number;
  price: {
    base: number;
    discounted: number;
  };
  thumbnail?: { url: string; fileId: string };
  images?: { url: string; fileId: string }[];
}
export interface ProductResponse {
  id: string;
  title: string;
  description: string;
  quantity: number;
  price: {
    base: number;
    discounted: number;
  };
  bulletPoints?: string[];
  images?: {
    url: string;
    fileId: string;
  }[];
  thumbnail: {
    url: string;
    fileId: string;
  };
  category: string;
}

export interface NewCartRequest {
  productId: string;
  quantity: number;
}
export interface NewFeaturedProduct {
  banner: {
    url: string;
    fileId: string;
  };
  link: string;
  linkTitle: string;
  title: string;
}

export interface FeaturedProductForUpdate {
  banner?: {
    url: string;
    fileId: string;
  };
  link: string;
  linkTitle: string;
  title: string;
}
export interface ReviewRequestBody {
  productId: string;
  comment?: string;
  rating: number;
}
export interface ReviewList {
  id: string;
  comment?: string;
  rating: number;
  date: Date;
  userInfo: {
    id: string;
    name: string;
    avatar?: string;
  };

  userId: { _id: ObjectId; name: string; avatar?: { url: string } };
}
