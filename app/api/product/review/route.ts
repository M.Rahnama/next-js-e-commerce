import startDb from "@/app/lib/db";
import ProductModel from "@/app/model/productModel";
import ReviewModel from "@/app/model/reviewModel";
import { ReviewRequestBody } from "@/app/types";
import { getServerAuthSession } from "@/auth";
import { isValidObjectId, Types } from "mongoose";
import { NextResponse } from "next/server";

export const POST = async (request: Request) => {
  try {
    const session = await getServerAuthSession();
    if (!session?.user) {
      return NextResponse.json(
        { error: "Sorry, not authorized" },
        { status: 401 }
      );
    }
    const { productId, rating, comment } =
      (await request.json()) as ReviewRequestBody;

    if (!isValidObjectId(productId[0])) {
      return NextResponse.json(
        { error: "Invalid proudct id" },
        { status: 401 }
      );
    }

    if (rating < 0 || rating > 5) {
      return NextResponse.json({ error: "Invalid rating" }, { status: 401 });
    }

    await startDb();

    const userId = session?.user.id;

    const data = {
      userId,
      rating,
      comment,
      product: productId[0],
    };
    await ReviewModel.findOneAndUpdate(
      { userId, product: productId[0] },
      data,
      {
        upsert: true,
      }
    );

    await averageRatingProduct(productId[0]);

    return NextResponse.json(
      { success: "Register review successful" },
      { status: 201 }
    );
  } catch (error) {
    NextResponse.json({ error: (error as any).message }, { status: 500 });
  }
};

const averageRatingProduct = async (productId: string) => {
  const [result] = await ReviewModel.aggregate([
    { $match: { product: new Types.ObjectId(productId) } },
    {
      $group: {
        _id: null,
        averageRating: { $avg: "$rating" },
      },
    },
  ]);

  if (result?.averageRating) {
    await ProductModel.findByIdAndUpdate(productId, {
      rating: result.averageRating,
    });
  }
};
