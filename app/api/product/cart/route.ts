import startDb from "@/app/lib/db";
import CartModel from "@/app/model/cartModel";
import { getServerAuthSession } from "@/auth";
import { NewCartRequest } from "@App/types";
import { isValidObjectId } from "mongoose";

import { NextResponse } from "next/server";

export const POST = async (request: Request) => {
  try {
    const session = await getServerAuthSession();
    const user = session?.user;
    if (!user)
      return NextResponse.json(
        { error: "Sorry, user not authorize" },
        { status: 401 }
      );

    const { productId, quantity } = (await request.json()) as NewCartRequest;

    if (!isValidObjectId(productId) || isNaN(quantity))
      return NextResponse.json(
        { error: "Sorry, Invalid request" },
        { status: 400 }
      );

    await startDb();
    const cart = await CartModel.findOne({ userId: user.id });
    if (!cart) {
      await CartModel.create({
        userId: user.id,
        items: [{ productId, quantity }],
      });
      return NextResponse.json({ success: true });
    }

    const existingProduct = cart.items.find(
      (item) => item.productId.toString() === productId
    );

    if (existingProduct) {
      existingProduct.quantity += quantity;

      if (existingProduct.quantity <= 0) {
        cart.items = cart.items.filter(
          (item) => item.productId.toString() !== productId
        );
      }
    } else {
      cart.items.push({ productId: productId as any, quantity });
    }
    await cart.save();
    return NextResponse.json({ success: "Add to cart successfuly" });
  } catch (error) {
    NextResponse.json({ error: (error as any).message }, { status: 500 });
  }
};
