import startDb from "@/app/lib/db";
import UserModel from "@/app/model/userModel";
import { SignInCredentials } from "@/app/types";
import { NextRequest, NextResponse } from "next/server";

export const POST = async (req: NextRequest) => {
  const { email, password } = (await req.json()) as SignInCredentials;

  if (!email || !password)
    return NextResponse.json({
      error: "Invalid request, email password missing",
    });
  await startDb();
  const user = await UserModel.findOne({ email });
  if (!user)
    return NextResponse.json({
      error: "Invalid request, Email/Password missing",
    });

  const matchPass = await user.comparePassword(password);
  if (!matchPass)
    return NextResponse.json({
      error: "Invalid request, Email/Password missmatch",
    });

  return NextResponse.json({
    user: {
      id: user._id.toString(),
      email: user.email,
      name: user.name,
      avatar: user.avatar,
      role: user.role,
      verified: user.verified,
    },
  });
};
