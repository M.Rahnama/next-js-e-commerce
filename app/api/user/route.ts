import { NewUser } from "@/app/types";
import startDb from "@Lib/db";
import UserModel from "@Model/userModel";
import { NextRequest, NextResponse } from "next/server";
import crypto from "crypto";
import emailVerificationToken from "@/app/model/emailVerificationToken";
import { sendEmail } from "@/app/lib/email";

export async function POST(request: NextRequest) {
  const body = (await request.json()) as NewUser;
  await startDb();
  const email = await UserModel.findOne({ email: body.email });
  const matchemail = await email?.compareEmail(body.email);
  if (matchemail)
    return NextResponse.json(
      {
        error: "Sorry, a user registered with this email",
      },
      { status: 400 }
    );
  const newUser = await UserModel.create({
    ...body,
  });

  const token = crypto.randomBytes(36).toString("hex");

  await emailVerificationToken.create({
    user: newUser._id,
    token,
  });

  const verificationUrl = `${process.env.VERIFICATION_URL}?token=${token}&userId=${newUser._id}`;

  await sendEmail({
    profile: { name: newUser.name, email: newUser.email },
    subject: "verification",
    linkUrl: verificationUrl,
  });

  return NextResponse.json({ message: "Please check your email" });
}
