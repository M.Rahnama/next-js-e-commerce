import startDb from "@/app/lib/db";
import { sendEmail } from "@/app/lib/email";
import passwordResetToken from "@/app/model/passwordResetToken";
import UserModel from "@/app/model/userModel";
import { UpdatePasswordRequest } from "@/app/types";
import { isValidObjectId } from "mongoose";
import { NextRequest, NextResponse } from "next/server";
import nodemailer from "nodemailer";

export const POST = async (req: NextRequest) => {
  const { password, token, userId } =
    (await req.json()) as UpdatePasswordRequest;

  try {
    if (!isValidObjectId(userId) || !token || !password) {
      NextResponse.json({ error: "Invalid request" }, { status: 401 });
    }
    await startDb();

    const resetToken = await passwordResetToken.findOne({ user: userId });
    if (!resetToken) {
      NextResponse.json({ error: "Invalid token" }, { status: 401 });
    }

    const matched = await resetToken?.compareToken(token);
    if (!matched)
      return NextResponse.json(
        { error: "Unauthorized request!" },
        { status: 401 }
      );

    const user = await UserModel.findById(userId);
    if (!user)
      return NextResponse.json({ error: "User not found!" }, { status: 404 });

    const isMatched = await user.comparePassword(password);
    if (isMatched) {
      return NextResponse.json(
        { error: "New password must be different!" },
        { status: 401 }
      );
    }

    user.password = password;
    await user.save();

    await passwordResetToken.findByIdAndUpdate(resetToken?._id);
    await sendEmail({
      profile: { name: user.name, email: user.email },
      subject: "passwordChanged",
    });

    return NextResponse.json({ message: "Your password has been changed" });
  } catch (error) {
    return NextResponse.json(
      {
        error: "Could not change password some thing is wrong.",
      },
      { status: 500 }
    );
  }
};
