import passwordResetToken from "@/app/model/passwordResetToken";
import UserModel from "@/app/model/userModel";
import { ForgetPassword } from "@/app/types";
import { NextRequest, NextResponse } from "next/server";
import crypto from "crypto";
import nodemailer from "nodemailer";
import startDb from "@/app/lib/db";
import { sendEmail } from "@/app/lib/email";

const checkError = (condition: boolean, message: string, status: number) => {
  if (condition) return NextResponse.json({ error: message }, { status });
};

export const POST = async (req: NextRequest) => {
  try {
    const { email } = (await req.json()) as ForgetPassword;
    if (!email)
      return NextResponse.json({ error: "Invalid email!" }, { status: 401 });

    await startDb();
    const user = await UserModel.findOne({ email });
    if (!user)
      return NextResponse.json({ error: "user not found!" }, { status: 404 });

    await passwordResetToken.findOneAndDelete({ user: user?._id });
    const token = crypto.randomBytes(36).toString("hex");
    await passwordResetToken.create({
      user: user?._id,
      token,
    });
    const resetPasswordLink = `${process.env.RESET_PASSWORD_URL}?token=${token}&userId=${user?._id}`;

    await sendEmail({
      profile: { name: user.name, email: user.email },
      subject: "forgetPassword",
      linkUrl: resetPasswordLink,
    });

    return NextResponse.json({ message: "Please check your email" });
  } catch (error) {
    return NextResponse.json(
      { error: (error as any).message },
      { status: 500 }
    );
  }
};
