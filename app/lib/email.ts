import nodemailer from "nodemailer";

type profile = { name: string; email: string };

interface EmailOptions {
  profile: profile;
  subject: "verification" | "forgetPassword" | "passwordChanged";
  linkUrl?: string;
}

const generateMailTransporter = () => {
  const transport = nodemailer.createTransport({
    host: "sandbox.smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "816919b604388d",
      pass: "5849a9ece1507e",
    },
  });
  return transport;
};

const sendEmailVerificationLink = async (profile: profile, linkUrl: string) => {
  const transport = generateMailTransporter();
  await transport.sendMail({
    from: "verification@mohsenCo.com",
    to: profile.email,
    html: `<h1>Please verify your email by clicking on <a href="${linkUrl}">this link</a> </h1>`,
  });
};

const sendForgetPasswordLink = async (profile: profile, linkUrl: string) => {
  const transport = generateMailTransporter();

  await transport.sendMail({
    from: "verification@mohsenCo.com",
    to: profile.email,
    html: `<h1>Click on <a href="${linkUrl}">this link</a> to reset your password.</h1>`,
  });
};

const sendUpdatePasswordConfirmation = async (profile: profile) => {
  const transport = generateMailTransporter();

  await transport.sendMail({
    from: "verification@mohsenCo.com",
    to: profile.email,
    html: `<h1>We changed your password <a href="${process.env.SIGN_IN_URL}">click here</a> to sign in.</h1>`,
  });
};

export const sendEmail = (options: EmailOptions) => {
  const { profile, subject, linkUrl } = options;

  switch (subject) {
    case "verification":
      return sendEmailVerificationLink(profile, linkUrl!);
    case "forgetPassword":
      return sendForgetPasswordLink(profile, linkUrl!);
    case "passwordChanged":
      return sendUpdatePasswordConfirmation(profile);
  }
};
