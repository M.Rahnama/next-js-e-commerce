import { getServerAuthSession } from "@/auth";
import { redirect } from "next/navigation";
import React, { ReactNode } from "react";
import EmailVerificationAlert from "../components/EmailVerificationAlert";
import Navbar from "../components/navbar";

type Props = {
  children: ReactNode;
};

const HomeLayout = async ({ children }: Props) => {
  const session = await getServerAuthSession();
  if (!session) return redirect("/auth/signin");
  return (
    <div className="max-w-screen-lg mx-auto p-4 xl:0">
      <Navbar />
      {children}
    </div>
  );
};

export default HomeLayout;
