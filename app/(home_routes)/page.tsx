import ProductCard from "../components/ProductCard";
import GridView from "../components/Gridview";
import FeaturedProductsSlider from "../components/FeaturedProductsSlider";
import { ProductType } from "../types";
import { fetchProducts } from "./action";
import { fetchBanner } from "../(admin)/products/featured/add/action";
import HorizontalMenu from "../components/HorizontalMenu";

export default async function Home() {
  const productList = await fetchProducts();
  const BannerList = await fetchBanner();

  const productListJson = JSON.parse(productList) as ProductType[];

  return (
    <div className="space-y-4">
      <FeaturedProductsSlider products={BannerList} />
      <HorizontalMenu />
      <GridView>
        {productListJson.map((product, inx) => {
          return <ProductCard key={inx} product={product} />;
        })}
      </GridView>
    </div>
  );
}
