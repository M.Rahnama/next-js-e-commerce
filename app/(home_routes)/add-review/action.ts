"use server";

import startDb from "@/app/lib/db";
import ReviewModel from "@/app/model/reviewModel";
import { getServerAuthSession } from "@/auth";

type ReviewDetails = {
  product: {
    title: string;
    thumbnail: { url: string };
  };
};
export const fetchReview = async (productId: string) => {
  try {
    const session = await getServerAuthSession();
    if (!session?.user) return null;

    await startDb();

    const result = await ReviewModel.findOne({
      userId: session.user.id,
      product: productId,
    }).populate<ReviewDetails>({
      path: "product",
      select: "title thumbnail.url",
    });

    if (result) {
      return {
        id: result._id.toString(),
        rainting: result.rating,
        comment: result.comment,
        product: {
          title: result.product.title,
          thumbnail: result.product.thumbnail.url,
        },
      };
    }
  } catch (error) {
    throw new Error((error as any).message);
  }
};
