import ReviewForm from "@Components/ReviewForm";
import React from "react";
import { fetchReview } from "../action";
import Image from "next/image";
type Props = {
  params: {
    pid: string;
  };
};

const ReviewFormPage = async (props: Props) => {
  const { pid } = props.params;
  const fetchDeatail = await fetchReview(pid);

  const initialValue = {
    rating: fetchDeatail?.rainting ?? 0,
    comment: fetchDeatail?.comment ?? "",
  };

  return (
    <div className="space-y-4">
      <div className="flex items-center">
        {fetchDeatail?.product && (
          <>
            <Image
              className="rounded"
              src={fetchDeatail?.product.thumbnail}
              alt={fetchDeatail?.product.title}
              height={50}
              width={50}
            />
            <span className="font-semibold pl-4">
              {fetchDeatail?.product.title ?? " "}
            </span>
          </>
        )}
      </div>
      <ReviewForm productId={pid} initialValue={initialValue} />
    </div>
  );
};

export default ReviewFormPage;
