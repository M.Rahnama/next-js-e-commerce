"use server";

import { isValidObjectId, ObjectId } from "mongoose";
import { redirect } from "next/navigation";
import ProductModel from "@Model/productModel";
import startDb from "@Lib/db";
import { ProductType, ReviewList } from "../types";
import ReviewModel from "../model/reviewModel";

const fetchProductHandler = async (category: string | null = null) => {
  await startDb();

  const product = category
    ? await ProductModel.find({ category }).sort("_createdAt").limit(20)
    : await ProductModel.find().sort("_createdAt").limit(20);

  const ProductList = product.map((product): ProductType => {
    return {
      id: product.id.toString(),
      title: product.title,
      description: product.description,
      category: product.category,
      thumbnail: product.thumbnail.url,
      price: product.price,
      sale: product.sale,
      rating: product.rating,
    };
  });

  return JSON.stringify(ProductList);
};

export const fetchProductById = async (productId: string) => {
  if (!isValidObjectId(productId)) return redirect("404");

  await startDb();
  const list = await ProductModel.findById(productId);

  if (!list) return redirect("404");

  const ProductInfo = {
    id: list.id.toString(),
    title: list.title,
    description: list.description,
    category: list.category,
    thumbnail: list.thumbnail.url,
    images: list.images?.map((image) => image.url),
    bulletPoint: list.bulletPoints,
    price: list.price,
    sale: list.sale,
    rating: list.rating,
  };

  return JSON.stringify(ProductInfo);
};
export const fetchProductReview = async (productId: string) => {
  await startDb();
  type PartialReviewList = Partial<ReviewList>;

  const ReviewsList = await ReviewModel.find({
    product: productId,
  }).populate<ReviewList>({
    path: "userId",
    select: "name avatar.url",
  });
  const ProductList = ReviewsList.map((review): PartialReviewList => {
    return {
      id: review._id.toString(),
      rating: review.rating,
      comment: review.comment,
      date: review.createdAt,
      userInfo: {
        id: review.userId._id.toString(),
        name: review.userId.name,
        avatar: review.userId.avatar?.url,
      },
    };
  });

  return JSON.stringify(ProductList);
};

export const fetchProducts = async () => {
  return await fetchProductHandler();
};

export const fetchProductsByCategory = async (category: string) => {
  return await fetchProductHandler(category);
};
