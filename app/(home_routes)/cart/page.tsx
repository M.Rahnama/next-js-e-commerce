import CartItems from "@/app/components/CartItems";
import React from "react";
import { fetchCartItems } from "./action";

const Cart = async () => {
  const cart = await fetchCartItems();
  if (!cart)
    return (
      <div className="py-4">
        <div className="mb-4">
          <h1 className="text-2xl font-semibold">Your Cart Details</h1>
          <hr />
        </div>
        <h1 className="text-center font-semibold text-2xl opacity-40 py-10">
          Your cart is empty!
        </h1>
      </div>
    );
  return <CartItems {...cart} />;
};

export default Cart;
