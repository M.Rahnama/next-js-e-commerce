import { ProductType } from "@App/types";
import { fetchProducts, fetchProductsByCategory } from "../../action";
import HorizontalMenu from "@Components/HorizontalMenu";
import GridView from "@Components/Gridview";
import ProductCard from "@Components/ProductCard";

type Props = {
  params: { category: string };
};

export default async function ProductByCategory({ params }: Props) {
  const productList = await fetchProductsByCategory(
    decodeURIComponent(params.category)
  );

  const productListJson = JSON.parse(productList) as ProductType[];

  return (
    <>
      <HorizontalMenu />
      {productListJson.length ? (
        <GridView>
          {productListJson.map((product, inx) => {
            return <ProductCard key={inx} product={product} />;
          })}
        </GridView>
      ) : (
        <h1 className="w-100 d-flex  rounded-md mt-7 text-blue-gray-800 font-semibold border border-blue-gray-200 p-5 justify-center text-center">
          Sorry, there is not any products here
        </h1>
      )}
    </>
  );
}
