import React from "react";
import { fetchProductById, fetchProductReview } from "../action";
import ProductView from "@Components/ProductView";
import ReviewsList from "@Components/ReviewsList";
import AlertBox from "@Components/AlertBox";

type ProductType = {
  title: string;
  description: string;
  images: string[];
  points?: string[];
  rating: number;
  price: { base: number; discounted: number };
  sale: number;
};
type Props = {
  params: {
    product: string[];
  };
};

const Product = async ({ params }: Props) => {
  const productId = params.product[1];
  const product = await fetchProductById(productId);
  const reviews = await fetchProductReview(productId);
  const productListJson = JSON.parse(product) as ProductType;
  const reviewsListJson = JSON.parse(reviews);

  return (
    <div className="p-4 space-y-4">
      <ProductView
        title={productListJson.title}
        description={productListJson.description}
        images={productListJson.images}
        price={productListJson.price}
        sale={productListJson.sale}
        points={productListJson.points}
        ratingAverage={productListJson.rating}
      />
      <div>
        <ReviewsList reviews={reviewsListJson} />
      </div>
      <AlertBox
        title="Please add your comment fot this product"
        hasLink
        href={`/add-review/${productId}`}
        linkText="Here"
        color="blue"
      />
    </div>
  );
};

export default Product;
